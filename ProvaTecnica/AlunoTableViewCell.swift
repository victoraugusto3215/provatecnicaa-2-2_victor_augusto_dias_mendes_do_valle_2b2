//
//  AlunoTableViewCell.swift
//  ProvaTecnica
//
//  Created by Eder Andrade on 26/04/22.
//

import UIKit

class AlunoTableViewCell: UITableViewCell {

    @IBOutlet weak var alunoLabel: UILabel!
    @IBOutlet weak var matriculaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
